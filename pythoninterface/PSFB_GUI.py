


import sys, os, serial, time,re,datetime
import matplotlib.pyplot as plt
import numpy as np
from tkinter import ttk as TTK
import tkinter as tk
from PIL import ImageTk, Image


###########Some Variables################
fbase = 50000000 #50MHz
tbase=1/fbase
td_counts = 9 #this is the # of counts @ 50MHz that equals the dead time of the mosfets (200ns)

# as an example let's asume we are trying to make a 500kHz square wave with about a 90 degrees phase shift


freq_counts = 99                #In order to have an even square wave, this needs to be odd. There are actually 100 counts total counting zero.
freq = fbase/(freq_counts+1)    #That means that we have 100 counts or 100 periods of a 50MHz signal. 50MHz/100 = 500kHz

fbase_display=(str(int(round(fbase/1000/(freq_counts+1),0)))+ " kHz")

freq_counts_half = (freq_counts+1)/2                #for example, if freq_counts = 99, there are actually 100 counts so you need to add 1 to 99 and then divide by 2 to get 50
phase_counts_max = freq_counts_half-td_counts       #You can't ever have a full 180 degrees of phase shift because of the dead time. If there was no dead time the 
                                                    #you could get full phase shift @ freq_counts_half
phase_counts = 25                                   #This value divided by freq_counts_half is the phase shift. In this case, phase_counts is 25 and freq_counts_half is 50  
phase_degrees = phase_counts/freq_counts_half*180   #that gives me a phase shift of 25/50*180 = 90 degrees

phase_max_degrees = phase_counts_max/freq_counts_half*180
COM='COM14'    # Need to add in a drop down menu to choose the COM
ser = serial.Serial(COM, baudrate=9600,timeout=.1)

initflag = 1

def ReadSerial():
    ser.write(str.encode('b*'))
    x=ser.readline() #read off the message
    LabelReadBackSerial.config(text=x)
    return()

def ScaleFreq():
    freq = fbase/(freq_counts+1) 
    freq_counts_half = (freq_counts+1)/2
    return()

def ScalePhase():
    phase_counts_max = freq_counts_half-td_counts
    phase_degrees = phase_counts/freq_counts_half*180
    return()

def ScaleAll():
    ScaleFreq()
    ScalePhase()
    return()
#phase_degrees = phase_counts/freq_counts_half*18
def UpdateActualPhase():
    '''global phase_counts
    global freq_counts
    global phase_degrees
    global freq_counts_half'''
    tdcounts=9
    phasecounts=int(EntryPhaseCounts.get())
    freqcounts=int(EntryFreqCounts.get())
    #tempvar=int(EntryPhaseMaxCounts.get())#I don't use this but if I don't include it I can't write the widget with a new value (Didn't work)
    freqcountshalf = (freqcounts+1)/2 

    phasedegrees = round(phasecounts/freqcountshalf*180,0)
    phasecountsmax = freqcountshalf-tdcounts
    phasemaxdegrees = round(phasecountsmax/freqcountshalf*180,0)

    EntryPhase.delete(0,tk.END)
    EntryPhase.insert(0,int(phasedegrees))

    EntryPhaseMaxCounts.delete(0,tk.END)
    EntryPhaseMaxCounts.insert(0,int(phasecountsmax))

    EntryPhaseMaxDegree.delete(0,tk.END)
    EntryPhaseMaxDegree.insert(0,int(phasemaxdegrees))
    print('made it into update actual phase')
    return()

def ManagePhase():
    if(AutoUpdatePhase):
        print("Does this work?")
        #EntryPhase.delete(0,tk.END) #Put Code here to update the phase
        #EntryPhase.insert(0,CalculatedPhaseValue)  #Calculate this alue whenever frequency is update
    return()

def UpdateActualFreq():
    print("I Love Thanksgiving")
    freqcounts=int(EntryFreqCounts.get())
    actualfreq = fbase/(1+freqcounts)/1000
    stringfreq=(str(int(round(actualfreq,0)))+ " kHz")
    EntryFreq.delete(0,tk.END)
    EntryFreq.insert(0,stringfreq)
    return()

def IncreaseFreq():

    currentfreq=int(EntryFreqCounts.get())
    print(type(currentfreq))
    incrementer=int(EntryIncrFreqCounts.get())
    updatedvalue = currentfreq-incrementer
    EntryFreqCounts.delete(0,tk.END)
    EntryFreqCounts.insert(0,updatedvalue)
    UpdateActualFreq()
    UpdateActualPhase()
    return()

def DecreaseFreq():
  
    currentfreq=int(EntryFreqCounts.get())
    print(type(currentfreq))
    incrementer=int(EntryIncrFreqCounts.get())
    updatedvalue = currentfreq+incrementer
    EntryFreqCounts.delete(0,tk.END)
    EntryFreqCounts.insert(0,updatedvalue)
    UpdateActualFreq()
    UpdateActualPhase()
    return()

def IncreasePhase():

    currentphase=int(EntryPhaseCounts.get())
   # print(type(currentfreq))
    incrementer=1
    updatedvalue = currentphase+incrementer
    EntryPhaseCounts.delete(0,tk.END)
    EntryPhaseCounts.insert(0,updatedvalue)
    UpdateActualPhase()
    return()   

def DecreasePhase():

    currentphase=int(EntryPhaseCounts.get())
   # print(type(currentfreq))
    incrementer=1
    updatedvalue = currentphase-incrementer
    EntryPhaseCounts.delete(0,tk.END)
    EntryPhaseCounts.insert(0,updatedvalue)
    UpdateActualPhase()
    return()  

def PadValue(valuetopad):
    temp=len(valuetopad)
    templist = ['0']*(7-temp)
    tempstring = ''.join(templist)
    valueout = tempstring+valuetopad
    print(valueout)
    return (valueout)

def ClearUART():
    x=ser.readline() #read off the message
    LabelReadBackData.config(text=x)
    return()

def SendUpdate():

    
    freqcounts=EntryFreqCounts.get()
    #freqcounts.decode("utf-8")
    phasecounts= EntryPhaseCounts.get()
#   phasecounts.decode("utf-8")

    freqsend=PadValue(freqcounts)
    
    phasesend=PadValue(phasecounts)
    ser.write(str.encode('f'+freqsend+'*'))
    print('p'+freqsend+'*\n')
    time.sleep(.25)
    ser.write(str.encode('p'+phasesend+'*'))
    print('p'+phasesend+'*')
    time.sleep(.25)
    ser.write(str.encode('u*')) 
    time.sleep(.25)
    x=ser.readline() #read off the message
    LabelReadBackData.config(text=x)
    return()

def SendCommand(event):
    ser.write( str.encode(EntryCommandSerial.get()))
    x=(ser.readline())  #read off the message which is in bytes 
    x.decode("utf-8")  #This is how you convert the bytes to a string
    
    y=(ser.readline()) #read off the message which is in bytes 
    y.decode("utf-8")  #This is how you convert the byts to a string
    x=x+y               #Concatenate the Strings
    LabelReadBackData.config(text=x)
    EntryCommandSerial.delete(0,tk.END)
    return()
   

master=tk.Tk() #This creates the master
#master.geometry('1000x700')
'''
phasevar = tk.StringVar()
phasevar.set('')
phasevar.trace('w',UpdateActualPhase)
'''


#**************ROW 0********************
LabelFreqInc = tk.Label(master, text = "Freq")
LabelFreqInc.grid(row=0,column=0,columnspan=2)

LabelPhaseInc = tk.Label(master, text = "Phase")
LabelPhaseInc.grid(row=0,column=2,columnspan=2)


LabelHeader = tk.Label(master, text = "Open Loop Frequency Adjuster")
LabelHeader.grid(row=0,column=4,columnspan=4)

#**************ROW 1********************

ButtonUp=tk.Button(master, text="Up", command = IncreaseFreq)
ButtonUp.grid(row=1,column=0)

ButtonDown=tk.Button(master, text="Down",command = DecreaseFreq)
ButtonDown.grid(row=1,column=1)

ButtonUp=tk.Button(master, text="Up", command = IncreasePhase)
ButtonUp.grid(row=1,column=2)

ButtonDown=tk.Button(master, text="Down",command = DecreasePhase)
ButtonDown.grid(row=1,column=3)

LabelFreqCounts = tk.Label(master,text="Freq Counts")
LabelFreqCounts.grid(row=1,column=4)

EntryFreqCounts=tk.Entry(master,justify='center')
EntryFreqCounts.grid(row=1,column=5)
EntryFreqCounts.insert(0,freq_counts)

LabelFreq = tk.Label(master,text="Freq ")
LabelFreq.grid(row=1,column=6)

EntryFreq=tk.Entry(master,justify='center')
EntryFreq.grid(row=1,column=7)
EntryFreq.insert(0,fbase_display)


#**************ROW 2********************

EntryIncrFreqCounts=tk.Entry(master,justify='center')
EntryIncrFreqCounts.grid(row=2,column=0,columnspan=2,sticky='nsew')
EntryIncrFreqCounts.insert(0,2)

EntryIncrPhaseCounts=tk.Entry(master,justify='center')
EntryIncrPhaseCounts.grid(row=2,column=2,columnspan=2,sticky='nsew')
EntryIncrPhaseCounts.insert(0,1)

LabelPhaseCounts = tk.Label(master,text="Phase Counts")
LabelPhaseCounts.grid(row=2,column=4)

EntryPhaseCounts=tk.Entry(master,justify='center')
EntryPhaseCounts.grid(row=2,column=5)
EntryPhaseCounts.insert(0,phase_counts)

LabelPhase = tk.Label(master,text="Phase")
LabelPhase.grid(row=2,column=6)

EntryPhase=tk.Entry(master,justify='center')
EntryPhase.grid(row=2,column=7)
EntryPhase.insert(0,phase_degrees)

#**************ROW 3********************

ButtonUpdate=tk.Button(master, text="Send Update",command = SendUpdate)
ButtonUpdate.grid(row=3,column=0)

ButtonUpdate=tk.Button(master, text="ReadSerial",command = ReadSerial)
ButtonUpdate.grid(row=3,column=1)

AutoUpdatePhase = tk.IntVar()
CheckButtonPhaseUpdate = tk.Checkbutton(master, text='AutoUpdate Phase', variable = AutoUpdatePhase, command = ManagePhase)
CheckButtonPhaseUpdate.grid(row=3,column=2,columnspan=2)

LabelPhaseMaxCounts = tk.Label(master,text="Max Phase (Counts)")
LabelPhaseMaxCounts.grid(row=3,column=4)
EntryPhaseMaxCounts = tk.Entry(master,justify='center')
EntryPhaseMaxCounts.grid(row=3,column=5)
EntryPhaseMaxCounts.insert(0,int(round(phase_counts_max,0)))

LabelPhaseMaxDegree = tk.Label(master,text="Max Phase (Degrees)")
LabelPhaseMaxDegree.grid(row=3,column=6)
EntryPhaseMaxDegree=tk.Entry(master,justify='center')
EntryPhaseMaxDegree.grid(row=3,column=7)
EntryPhaseMaxDegree.insert(0,int(round(phase_max_degrees,0)))



#*************Row 4***************************
btn_quit = tk.Button(master,text='Quit', bd=1, command = master.destroy)
btn_quit.grid(row=4,column=0, sticky='nsew',columnspan=8)

#*************Row5*************************************
LabelSendCommandSerial = tk.Label(master,text='Command')
LabelSendCommandSerial.grid(row=5,column = 0)

EntryCommandSerial = tk.Entry(master,width=109)
EntryCommandSerial.grid(row=5,column=1,columnspan=7)
EntryCommandSerial.bind('<Return>',SendCommand)
#*************Row6*************************************
LabelReadBackSerial = tk.Label(master,text='Readback')
LabelReadBackSerial.grid(row=6,column = 0)

LabelReadBackData = tk.Label(master)
LabelReadBackData.grid(row=6,column=1,columnspan=7)

#Row 7
btn_clearuart = tk.Button(master,text='Clear Read Buffer', bd=1, command = ClearUART)
btn_clearuart.grid(row=7,column=0, sticky='nsew',columnspan=8)
master.mainloop()