/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *


                    Cy_SCB_UART_Put(UART_HW,parameter);
                    Cy_SCB_UART_PutString(UART_HW,"\nThere is no parameter to parse here!\n");
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"
uint16 phase=30;
uint16 td=10;
uint16 ts=99;
uint16 compareval=50;
//compareval =(ts+1)/2;
#define TRANSMITBUFFERSIZE 32
#define PARAMETERSIZE 7
//uint32_t TransmitBuffer[TRANSMIT_BUFFER_SIZE];//We need this to interface with the API PWM calls and to output it to the UART
char parameterarray[PARAMETERSIZE];//This is what I will use to parse the sent commands. I am creating an array of Uint16. 
                                //let's say the gui sends in "u200" then I will store the "200" in this array as 000200


void handle_error(void);
void ISR_UART(void);

#define LED_ON  (0x00u)
#define LED_OFF (!LED_ON)
uint32_t read_data;
uint32_t data_received;
uint8_t uart_error;

/*UART State Machine Constants*/
uint32    parameter=0;
uint8  loopcounter=0;
char   command=0; //This is similar to the parameter variable. In here I will store the command the user has sent.
char   Ch;        /* Variable to store UART received character */
char   tempchar;  //temporary storage variable
char tempcommand = 0;
volatile uint16 interruptcounter=0; //This is used to fill the parameter array
char TransmitBuffer[TRANSMITBUFFERSIZE];
void handle_error(void)
{
     /* Disable all interrupts */
    __disable_irq();
    
    /* Switch on error LED */
    Cy_GPIO_Write(RED_LED_ERROR_0_PORT, RED_LED_ERROR_0_NUM, LED_ON);
    while(1u) {}
}


int ConvertParamCharToUint()//This function sorts through the parameter array and subtracts off 48. For example, if the user sent "u200", then the
                            //200 in ascii is actually 50 48 48
{
    int fullnum=0;
    for(int i=0;i<=PARAMETERSIZE-1;i++)
    {
       fullnum=fullnum+(parameterarray[i]-48)*pow(10,i);//This looks complicated, but I am just trying to build a number out of the entered characters. 
    }                                           //on iteration 1, I am in the ones place, so I just want to multiple by 1. On the second iteration I 
                                               //am on the 10s digit so I want to multiply by 10^1 or 10. If I sum as I go, I'll rebuild the number
    return fullnum;
}

void ClearParameterArray() //all this function does is zero out the parameter array. 
{
    for (int i=0; i<PARAMETERSIZE;i++)
    {
        parameterarray[i]=48;//This is zero in ascii or char world. So subtract off 48 to get the actual #
    }
    
}




void ISR_UART(void)
{
    
  if((UART_HW->INTR_RX_MASKED & SCB_INTR_RX_MASKED_NOT_EMPTY_Msk ) != 0)
	{
        /* Clear UART "RX fifo not empty interrupt" */
		UART_HW->INTR_RX = UART_HW->INTR_RX & SCB_INTR_RX_NOT_EMPTY_Msk;       
        tempchar=Cy_SCB_UART_Get(UART_HW);  //On the interrupt grab a char. Imagine we are sending a000046*. 

        if(interruptcounter==0) //If this is the first interrupt, we know char 0 is our command. In this case the char: a
                {  
                   tempcommand = tempchar; //we don't put it into the command variable till the end
                   interruptcounter++; //increment so we know we're after the command char
                }     
        else if (tempchar=='*')
                {
                    command=tempcommand;    //  Once we see the asterisk we can load a into the command variable 
                                            // to execute the main.c case statement 
                    interruptcounter=0;     //  Get the counter ready for the next command
                }
        else
                {
                parameterarray[PARAMETERSIZE-interruptcounter]=tempchar; //if it's not a command or an asterisk
                interruptcounter++;                                     //it's part of the paramter and needs to be stored appropriately
                }                                                   //This will build the an array that has our parameter in it. At the end, our
                                                                    //paramterarray will be full of: 0 0 0 0 4 6 
    }            
}

void StopPWMs()
{
    Cy_TCPWM_TriggerStopOrKill(TCPWM0, PWM_A_CNT_MASK);
    Cy_TCPWM_TriggerStopOrKill(TCPWM0, PWM_B_CNT_MASK);
    Cy_TCPWM_TriggerStopOrKill(TCPWM0, PWM_C_CNT_MASK);
    Cy_TCPWM_TriggerStopOrKill(TCPWM0, PWM_D_CNT_MASK);
    /*Cy_TCPWM_PWM_Disable(TCPWM0,PWM_A_CNT_MASK);
    Cy_TCPWM_PWM_Disable(TCPWM0,PWM_B_CNT_MASK);
    Cy_TCPWM_PWM_Disable(TCPWM0,PWM_C_CNT_MASK);
    Cy_TCPWM_PWM_Disable(TCPWM0,PWM_D_CNT_MASK);
    */
}

void UpdatePhase()
{
    parameter= ConvertParamCharToUint();
    phase =  parameter;
    /*sprintf(TransmitBuffer,"\nPhaseNow: %d\n",phase);
    Cy_SCB_UART_PutString(UART_HW,TransmitBuffer);*/
    parameter=0;
}

void UpdatePeriod()
{
    parameter= ConvertParamCharToUint();
    ts =  parameter;
    compareval=(ts+1)/2;
    /*sprintf(TransmitBuffer,"\nPeriod Now: %d  CompareVal Now: %d\n",ts,compareval);
    Cy_SCB_UART_PutString(UART_HW,TransmitBuffer);*/
    parameter=0;
}

void UpdatePWMs()
{
    
    Cy_TCPWM_Enable_Multiple(TCPWM0, PWM_A_CNT_MASK | PWM_B_CNT_MASK |  PWM_C_CNT_MASK | PWM_D_CNT_MASK);
    
    Cy_TCPWM_PWM_SetPeriod0(TCPWM0, PWM_A_CNT_NUM, ts);
    Cy_TCPWM_PWM_SetPeriod0(TCPWM0, PWM_B_CNT_NUM, ts);
    Cy_TCPWM_PWM_SetPeriod0(TCPWM0, PWM_C_CNT_NUM, ts);
    Cy_TCPWM_PWM_SetPeriod0(TCPWM0, PWM_D_CNT_NUM, ts);
    
    Cy_TCPWM_PWM_SetCompare0(TCPWM0, PWM_A_CNT_NUM, compareval-td);
    Cy_TCPWM_PWM_SetCompare0(TCPWM0, PWM_B_CNT_NUM, compareval-td);
    Cy_TCPWM_PWM_SetCompare0(TCPWM0, PWM_C_CNT_NUM, compareval-td);
    Cy_TCPWM_PWM_SetCompare0(TCPWM0, PWM_D_CNT_NUM, compareval-td);
    
    Cy_TCPWM_PWM_SetCounter(PWM_A_HW, PWM_A_CNT_NUM, ts-td);
    Cy_TCPWM_PWM_SetCounter(PWM_B_HW, PWM_B_CNT_NUM, 0); 
    Cy_TCPWM_PWM_SetCounter(PWM_C_HW, PWM_C_CNT_NUM, ts-td-phase); 
    Cy_TCPWM_PWM_SetCounter(PWM_D_HW, PWM_D_CNT_NUM, phase);
}

void RestartPWMs()
{
    Cy_TCPWM_TriggerStart(TCPWM0, PWM_A_CNT_MASK | PWM_B_CNT_MASK |  PWM_C_CNT_MASK | PWM_D_CNT_MASK);
}


int main(void)
{  
    /***************************************UART Setup************************************************/
     cy_en_scb_uart_status_t init_status;
        
    /* Start UART operation. */
    init_status = Cy_SCB_UART_Init(UART_HW, &UART_config, &UART_context);
    if(init_status!=CY_SCB_UART_SUCCESS)
    {
        handle_error();
    }
    Cy_SCB_UART_Enable(UART_HW);   

    /* Transmit header to the terminal. */
    Cy_SCB_UART_PutString(UART_HW, "\r\n**********   We got some COM!!!  ***************************************\r\n");
       
    
    /* Unmasking only the RX fifo not empty interrupt bit */
    UART_HW->INTR_RX_MASK = SCB_INTR_RX_MASK_NOT_EMPTY_Msk;
    
    /* Interrupt Settings for UART */    
    Cy_SysInt_Init(&UART_SCB_IRQ_cfg, ISR_UART);
    
    /* Enable the interrupt */
    NVIC_EnableIRQ(UART_SCB_IRQ_cfg.intrSrc);
    
    /* Initialize flags */
    data_received = 0;
    uart_error = 0;  
    

/*******************PWM SETUP CODE HERE********************************************/
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    (void) Cy_TCPWM_PWM_Init(PWM_A_HW, PWM_A_CNT_NUM, &PWM_A_config);
	(void) Cy_TCPWM_PWM_Init(PWM_B_HW, PWM_B_CNT_NUM, &PWM_B_config);
    (void) Cy_TCPWM_PWM_Init(PWM_C_HW, PWM_C_CNT_NUM, &PWM_C_config);
    (void) Cy_TCPWM_PWM_Init(PWM_D_HW, PWM_D_CNT_NUM, &PWM_D_config);
    
    Cy_TCPWM_Enable_Multiple(TCPWM0, PWM_A_CNT_MASK | PWM_B_CNT_MASK |  PWM_C_CNT_MASK | PWM_D_CNT_MASK);
    
    //Decide where each counter will start. 
    Cy_TCPWM_PWM_SetCounter(PWM_A_HW, PWM_A_CNT_NUM, ts-td); //This one needs to start backwards the amount of the dead time. If the counter goes up to
                                                            //100, and the dead time is 10, this will start it @ 90 when it is low. If B starts @ 0, then 
                                                            //we have the situation 
    Cy_TCPWM_PWM_SetCounter(PWM_B_HW, PWM_B_CNT_NUM, 0); 
    Cy_TCPWM_PWM_SetCounter(PWM_C_HW, PWM_C_CNT_NUM, ts-td-phase); 
    Cy_TCPWM_PWM_SetCounter(PWM_D_HW, PWM_D_CNT_NUM, phase); 
    
    //Start all the PWMs at the same time
    Cy_TCPWM_TriggerStart(TCPWM0, PWM_A_CNT_MASK | PWM_B_CNT_MASK |  PWM_C_CNT_MASK | PWM_D_CNT_MASK);
    
    
    /****************************End PWM Setup Code******************************************************************/
    
        /* Enable global interrupts. */
    __enable_irq(); /* Enable global interrupts. */
    
    
    
    for(;;)
    {
switch(command)
        {
            case 0:
                break;
            case 'a':

                    parameter= ConvertParamCharToUint();
                    Cy_SCB_UART_PutString(UART_HW,"Your Parameter is: ");
                    sprintf(TransmitBuffer,"%lu", parameter);
                    Cy_SCB_UART_PutString(UART_HW,TransmitBuffer);
                    ClearParameterArray();

                    command=0;
                    interruptcounter=0;//why do I need this? I thought I was changing this is the interrupt routine itself.
                    
                break;
                case 'b':
                    Cy_SCB_UART_PutString(UART_HW,"There is no parameter to parse here!");
                    command=0;
                  
                break;        
                case 'c':
                     Cy_SCB_UART_PutString(UART_HW,"We're in the money!");
                     command=0;
                 
                break;
                case 'p':
                    UpdatePhase();
                    command=0;
                break;
                case 'f':
                    UpdatePeriod();
                    command=0;
                    
                break;
                case 'u':
                    
                    StopPWMs();
                    UpdatePWMs();
                    RestartPWMs();
                    sprintf(TransmitBuffer,"Period: %d | Phase: %d \n",ts,phase);
                    Cy_SCB_UART_PutString(UART_HW,TransmitBuffer);
                    command=0;
                break;
                case 'g':
                break;
                case 'h':
                break;
                case 'i':
                break;
        
        }
    }    
        
        
        
        /* Place your application code here. */
    
}

/* [] END OF FILE */
